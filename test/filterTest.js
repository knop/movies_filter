var assert = require('assert');
var filter = require('../filter');
var fs     = require('fs');

exports['test filter.process'] = function(assert) {
  var inputObj = JSON.parse(fs.readFileSync('./test/testRequest.json'));
  var outputObj = filter.process(inputObj);  
  assert.equal(outputObj.response.length, 1, 'array filtered');
  assert.equal(outputObj.response[0].title, 'Toy Hunter', 'title set');
  assert.equal(outputObj.response[0].slug, 'show', 'slug set');
  assert.equal(outputObj.response[0].image, 'http://test.url/image.jpg', 'image url set');
}
 
if (module == require.main) require('test').run(exports);
