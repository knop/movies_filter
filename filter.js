var _ = require('underscore');

var isProperMovie = function(movie) {
  return movie.drm && movie.episodeCount > 0;
}

var convert = function(movie) {
  return _.extend({"image": movie.image.showImage}, _.pick(movie, 'slug', 'title'));
}

exports.process = function(inputJSON) {
  var filtered = _.filter(inputJSON.payload, isProperMovie);
  return {"response": _.map(filtered, convert)};
}
